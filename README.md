# docker-qt-gcc (with static build)

This docker image has been created to build **static** 64 bit [Qt](https://www.qt.io)
desktop applications with GitLab's CI pipeline.
Of course you can also use it outside of GitLab's CI pipeline.
The image is based on [Debian](https://hub.docker.com/_/debian/).
Please find prebuilt images in the [container registry](/../container_registry).

The Qt binary files' location depends on the Qt version that was installed.
`PATH` is adjusted to contain that location.

# Image Versioning

The image version is derived from the version of Qt being used.
For example all images using Qt version 5.7 have a version of "5.7.rX" where X
can be any number bigger than zero.
Please see the version table below for details on current images.

## Version Table

All images are available in the [container registry](/../container_registry).

| image     | from           | gcc     | QMake | Qt       | Qt binaries path            |
|-----------|----------------|---------|-------|----------|-----------------------------|
| `latest`  | `stretch-slim` | `6.3.0` | `3.1` | `5.12.0` | `/usr/local/Qt-5.12.0/bin`  |
| `5.12.r1` | `stretch-slim` | `6.3.0` | `3.1` | `5.12.0` | `/usr/local/Qt-5.12.0/bin`  |
| `5.12.r2` | `stretch-slim` | `6.3.0` | `3.1` | `5.12.1` | `/usr/local/Qt-5.12.1/bin`  |

# Change History

## 5.12.r2

- Use Qt version 5.12.1.
- Add ssl support

## 5.12.r1

- Use Qt version 5.12.0.