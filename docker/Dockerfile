##
## Docker image for compiling static QT desktop applications
##

FROM debian:stretch-slim

ENV QT_TMP_PATH="/tmp/qt"
ENV PATH="${PATH}:/usr/local/Qt-5.12.1/bin"
ENV QT_SCRIPT_PATH="/usr/local/bin"
ENV QT_SRC_ARCHIVE="qt-everywhere-src-5.12.1.tar.xz"

ADD qt-make-test.sh ${QT_SCRIPT_PATH}/qt-make-test.sh

RUN QT_INSTALLATION_DEPS='curl ca-certificates xvfb xauth libx11-xcb1 libfontconfig1 libdbus-1-3 openssl libgl1-mesa-dev libglib2.0-0 xz-utils build-essential libpulse-dev g++ make python libpq-dev libssl-dev' \
    && apt-get update \
    && apt-get -qq install --no-install-recommends $QT_INSTALLATION_DEPS \
\
    && mkdir ${QT_TMP_PATH} \
    && echo ${QT_TMP_PATH}/${QT_SRC_ARCHIVE} \
    && curl -Lo ${QT_TMP_PATH}/${QT_SRC_ARCHIVE} http://master.qt.io/archive/qt/5.12/5.12.1/single/${QT_SRC_ARCHIVE} \
    && cd ${QT_TMP_PATH} \
    && tar -xpJf ${QT_SRC_ARCHIVE} \
    && cd qt-everywhere-src-5.12.1 \
\
    && ./configure -confirm-license -opensource -static -nomake examples -nomake tools -make libs -no-rpath -skip qtwebengine -sql-psql \
    && CORES="$(grep -c ^processor /proc/cpuinfo)" \
    && make -j${CORES} \
    && make install -j${CORES} \
\
    && rm -rf /tmp/* \
    && apt-get -qq purge --auto-remove $QT_INSTALLATION_DEPS \
    && apt-get -qq install --no-install-recommends libgl1-mesa-dev libglib2.0-0 libpulse-dev g++ make libpq-dev openssl libssl-dev \
\
    && chmod +x ${QT_SCRIPT_PATH}/qt-make-test.sh